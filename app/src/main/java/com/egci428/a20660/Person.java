package com.egci428.a20660;

/**
 * Created by VerifyFX on 22/11/2016.
 */

public class Person {
    private String name;
    private Double lat, lon;

    public Person() {

    }
    public Person(String name, Double lat, Double lng) {
        this.name = name;
        this.lat = lat;
        this.lon = lng;
    }

    public Person(String name, String lat, String lon) {
        this.name = name;
        this.lat = Double.parseDouble(lat);
        this.lon = Double.parseDouble(lon);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLng(Double lng) {
        this.lon = lng;
    }
}
