package com.egci428.a20660;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by VerifyFX on 12/11/2016.
 */

public class UsersAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Person> mDataSource;

    public UsersAdapter(Context context, List<Person> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public  int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int pos) {
        return mDataSource.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.list_row, parent, false);
        TextView lbluser = (TextView) rowView.findViewById(R.id.lblUser);

        Person person = (Person) getItem(pos);
        lbluser.setText(person.getName());

        return rowView;
    }
}