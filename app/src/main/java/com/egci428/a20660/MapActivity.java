package com.egci428.a20660;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private String name = null;
    private double lat = 0;
    private double lon = 0;

    private MapFragment mapFragment;
    private Toolbar toolbar;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null) {
            name = bundle.getString(HomeActivity.EXTRA_NAME);
            lat = bundle.getDouble(HomeActivity.EXTRA_LAT);
            lon = bundle.getDouble(HomeActivity.EXTRA_LON);
            Log.d("MAPACT", name+": ["+lat+", "+lon+"]");
        }

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(name+"'s Location");
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng location = new LatLng(lat, lon);
        map.addMarker(new MarkerOptions()
                .position(location)
                .title(name));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
